﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Fizykochemia
{
    public partial class MainView : Form
    {
        private List<int> temp;
        private List<double> specificHeat;
        private Dictionary<int, double> enthalpy;
        private Dictionary<int, double> shForTemp = new Dictionary<int, double>();
        private PlotModel plotModel = new PlotModel();
        private Stack<OxyColor> colors = new Stack<OxyColor>();
        private List<Dictionary<int, double>> wykresy = new List<Dictionary<int, double>>();
        private int minTemp;
        private int maxTemp;
        private string path = "C:\\Users\\ggork\\OneDrive\\Pulpit\\Specific_Heat.txt";
        
        public MainView()
        {
            InitializeComponent();
            comboBox1.Items.Add(HeatEffectDistribution.Rosnacy);
            comboBox1.Items.Add(HeatEffectDistribution.Staly);
            comboBox1.Items.Add(HeatEffectDistribution.Malejacy);
            comboBox1.Items.Add(HeatEffectDistribution.Kwadratowy);
            comboBox1.Items.Add(HeatEffectDistribution.Sigmoidalny);
            MaximumSize = Size;
            MinimumSize = Size;
            Init();
        }

        private void Distribute(ref Dictionary<int, double> ent, double additionalHeatEffect, int startTemp, int endTemp = -1, HeatEffectDistribution distribution = HeatEffectDistribution.Rosnacy)
        {
            for(int i=0; i<ent.Count; i++)
            {
                //ent[ent.Keys.ElementAt(i)] = 100;
            }

            if(endTemp < startTemp)
            {
                endTemp = ent.Keys.ElementAt(ent.Count-1);
            }
            
            switch (distribution)
            {
                case HeatEffectDistribution.Rosnacy:

                    /*
         factor = 0
        factor_inc = value / (t_end - t_start)
        for index in range(len(data["temp"])):
            if (t_start <= data["temp"][index]) and (t_end >= data["temp"][index]):
                data["enthalpy"].at[index] += factor
                factor += factor_inc
            elif data["temp"][index] > t_end:
                data["enthalpy"].at[index] += factor
        return data
             
             */

                    double factor = 0;
                    var factor_inc = additionalHeatEffect / (endTemp - startTemp);

                    for (int i = 0; i < ent.Count; i++)
                    {
                        int t = ent.Keys.ElementAt(i);
                        if (startTemp <= t && endTemp >= t)
                        {
                            ent[t] += factor;
                            factor += factor_inc;
                        }
                        else if(t > endTemp)
                        {
                            ent[t] += factor;
                        }
                    }

                    /*
                    int cos = endTemp - startTemp;
                    double dH1 = additionalHeatEffect / cos;
                    
                    double suma1 = 0;
                    double toAdd1 = 0;
                    Dictionary<int, double> dodane = new Dictionary<int, double>();

                    for (int i = startTemp; i <= ent.Keys.Max(); i++)
                    {
                        int key = i;
                        if (key > startTemp && key <= endTemp)
                        {
                            toAdd1 +=  dH1;
                            dodane.Add(key, ent[key]);
                            ent[key] += toAdd1;
                            suma1 += toAdd1;
                        }
                        else if(key > endTemp)
                        {
                            ent[key] += toAdd1;
                        }
                    }
                    */
                    break;
                case HeatEffectDistribution.Staly:
                    double dH2 = additionalHeatEffect / (endTemp - startTemp);

                    for (int i = startTemp; i <= ent.Keys.Max(); i++)
                    {
                        ent[i] += dH2;
                    }
                    break;
                case HeatEffectDistribution.Malejacy:
                    double dH = additionalHeatEffect / (endTemp-startTemp);
                    double dhl = additionalHeatEffect / (GaussSum(endTemp - startTemp) - 1);
                    int counter = 0;
                    double suma = 0;
                    double toAdd = 0;
                    int dx = endTemp - startTemp;
                    for (int i = startTemp; i <= ent.Keys.Max(); i++)
                    {
                        int key = i;
                        if (key > startTemp && key <= endTemp)
                        {
                            counter++;
                            toAdd += (dx - counter) * dhl;
                            ent[key] += toAdd;
                            suma += toAdd;
                        }
                        else
                        {
                            ent[key] += toAdd;
                        }
                    }
                    break;

                case HeatEffectDistribution.Kwadratowy:
                    double factor2 = -1;
                    int counter2 = 0;
                    var factor_inc2 = additionalHeatEffect / (endTemp - startTemp);
                    var dt = endTemp - startTemp;
                    var dtsr = (endTemp + startTemp) / 2;
                    for(int i=0; i<ent.Count; i++)
                    {
                        int t = ent.Keys.ElementAt(i);
                        if(startTemp < t && (endTemp)>= t)
                        {
                            factor2++;
                            ent[t] += additionalHeatEffect * (factor2*factor2) / (dt*dt);
                        }
                        else if(t > endTemp)
                        {
                            ent[t] += additionalHeatEffect;
                        }
                    }

                    break;
                case HeatEffectDistribution.Sigmoidalny:
                    double factor3 = -1;
                    int counter3 = 0;
                    var factor_inc3 = additionalHeatEffect / (endTemp - startTemp);
                    var dt3 = endTemp - startTemp;
                    var dtsr3 = (endTemp + startTemp) / 2;
                    double start = 0;
                    for (int i = 0; i < ent.Count; i++)
                    {
                        int t = ent.Keys.ElementAt(i);
                        if (startTemp < t && (endTemp) >= t)
                        {
                            factor3++;
                            start = Map(t, startTemp, endTemp, -10, 10);
                            double add = Math.Exp(start) / (Math.Exp(start) + 1);
                            ent[t] += additionalHeatEffect * add;
                        }
                        else if (t > endTemp)
                        {
                            ent[t] += additionalHeatEffect;
                        }
                    }

                    break;
            }
        }

        public double Map(double value, double fromSource, double toSource, double fromTarget, double toTarget)
        {
            return (value - fromSource) / (toSource - fromSource) * (toTarget - fromTarget) + fromTarget;
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            OnLoad();
            
        }

        private void OnLoad()
        {
            plotView1.Model = plotModel;

            LoadData();
            FillColors();
            minTemp = temp[0];
            maxTemp = temp.Last();
            CalculateAll();
            enthalpy = CalculateEnthalpy();

            DodajWykres(enthalpy);

            foreach(DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.IsNewRow) continue;

                var cells = row.Cells;

                numTemp.Value = (int)cells["TempStart"].Value;
                numEnt.Value = decimal.Parse(cells["EfektCieplny"].Value.ToString());
                comboBox1.SelectedItem = (HeatEffectDistribution)cells["Rozklad"].Value;
                checkBox1.Checked = ((bool)cells["Cb1"].Value);
                numTempKonc.Value = (int.Parse(cells["TempKonc"].Value.ToString()));
                btnAdd_Click(null, null);
            }
        }

        private void CalculateAll()
        {
            for(int i=minTemp; i<maxTemp; i++)
            {
                if (shForTemp.ContainsKey(i))
                {
                    continue;
                }
                shForTemp.Add(i, Lerp(i));
            }
            shForTemp = shForTemp.OrderBy(key => key.Key).ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
        }

        private void Init()
        {
            temp = new List<int>();
            specificHeat = new List<double>();
        }

        private double Lerp(int t)
        {
            double result = 0;
            int t1 = temp.LastOrDefault(x => x < t);        //56
            int t2 = temp.FirstOrDefault(x => x > t);       //96
            
            double sh1 = specificHeat[temp.IndexOf(t1)];
            double sh2 = specificHeat[temp.IndexOf(t2)];

            double deltaSH = sh2 - sh1;   //
            double deltaT = t2 - t1;                        //40

            double delta = deltaSH / deltaT;

            result = sh1 + (t - t1) * delta;
            //0.449636
            return result;
        }

        private Dictionary<int, double> CalculateEnthalpy()
        {
            temp = shForTemp.Select(x => x.Key).ToList();
            specificHeat = shForTemp.Select(x => x.Value).ToList();
            var result = new Dictionary<int, double>();

            foreach(var a in shForTemp)
            {
                if(temp.IndexOf(a.Key) == 0)
                {
                    result.Add(temp[0], 0);
                    continue;
                }
                var t1 = a.Key-1;
                var t2 = shForTemp[a.Key];
                var t3 = shForTemp[a.Key - 1];
                var t4 = a.Key;
                var t5 = a.Key - 1;
                double _enthalpy = result[t1] + (t2 + t3) / 2 * (t4 - t5);
                result.Add(a.Key, _enthalpy);
            }
            wykresy.Add(result);
            return result;
        }

        private void LoadData()
        {
            wykresy = new List<Dictionary<int, double>>();
            temp = new List<int>();
            specificHeat = new List<double>();
            enthalpy = new Dictionary<int, double>();
            shForTemp = new Dictionary<int, double>();
            using (var reader = new StreamReader(path))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Replace('.',',');
                    var values = line.Split(' ');
                    
                    if(values.Length == 1)
                    {
                        int count = 0;
                        if(int.TryParse(values[0], out count))
                        {
                            for(int i = 0; i < count; i++)
                            {
                                var line2 = reader.ReadLine().Replace('.', ',');
                                var values2 = line2.Split(',');

                                int temp = 0;
                                int.TryParse(values2[0], out temp);
                                double cw = 0;
                                double.TryParse(values2[2], out cw);
                                HeatEffectDistribution heat = HeatEffectDistribution.Rosnacy;
                                switch(values2[3])
                                {
                                    case "Rosnacy":
                                        heat = HeatEffectDistribution.Rosnacy;
                                        break;
                                    case "Staly":
                                        heat = HeatEffectDistribution.Staly;
                                        break;
                                    case "Malejacy":
                                        heat = HeatEffectDistribution.Malejacy;
                                        break;
                                    case "Kwadratowy":
                                        heat = HeatEffectDistribution.Kwadratowy;
                                        break;
                                    case "Sigmoidalny":
                                        heat = HeatEffectDistribution.Sigmoidalny;
                                        break;
                                }
                                bool cb = false;
                                bool.TryParse(values2[4], out cb);
                                int tempKonc = 0;
                                int.TryParse(values2[1], out tempKonc);

                                dataGridView1.Rows.Add(temp, tempKonc, cw, heat, cb);
                            }
                        }
                    }
                    else if (values.Length == 2)
                    {
                        double _tempRaw = 0;
                        int _temp;
                        double _sh = 0;
                        if(double.TryParse(values[0], out _tempRaw) && double.TryParse(values[1], out _sh))
                        {
                            _temp = (int)_tempRaw;
                            temp.Add(_temp);
                            specificHeat.Add(_sh);
                            shForTemp.Add(_temp, _sh);
                        }
                    }
                }
            }
            ConfigureForm();
        }

        private void ConfigureForm()
        {
            int t = temp.First();
            numTemp.Minimum = t;
            numTemp.Value = numTemp.Minimum;
            numTemp.Maximum = temp.Last();
            numTempKonc.Maximum = temp.Last();
            numEnt.Maximum = 999999;
            comboBox1.SelectedIndex = 0;
            
            plotView1.Model.Series.Clear();
        }

        private void otwórzToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Plik tekstowy (.txt)|*.txt";
            DialogResult dialog = MessageBox.Show("Czy chcesz otworzyć bez zapisania?", "Otwórz", MessageBoxButtons.YesNoCancel);
            if(dialog == DialogResult.Yes)
            {
                Load2(openFileDialog);
            }
            else if(dialog == DialogResult.No)
            {
                Save();
                Load2(openFileDialog);
            }
        }

        private void Load2(OpenFileDialog openFileDialog)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog.FileName;
                OnLoad();//LoadData();
            }
        }

        private void zapiszToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private bool Save()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Plik tekstowy (.txt)|*.txt";
            DialogResult dialogResult = saveFileDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                var przemiany = dataGridView1.Rows;
                int count = przemiany.Count-1;
                string content = "";

                content += count + Environment.NewLine;

                foreach (DataGridViewRow row in przemiany)
                {
                    if (row.IsNewRow) continue;

                    content += row.Cells["TempStart"].Value.ToString() + "," + row.Cells["TempKonc"].Value.ToString() + "," + row.Cells["EfektCieplny"].Value.ToString() + "," + (row.Cells["Rozklad"].Value.ToString() == "True" ? 1 : 0) + "," + row.Cells["Cb1"].Value.ToString() + Environment.NewLine;
                }
                foreach (var a in shForTemp)
                {
                    content += a.Key + " " + a.Value + Environment.NewLine;
                }
                File.AppendAllText(saveFileDialog.FileName, content);

            }
            return dialogResult == DialogResult.OK;
        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainView_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = CloseProgram(e);
        }

        private bool CloseProgram(FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Czy chcesz zamknąć bez zapisania?", "Zamknij", MessageBoxButtons.YesNoCancel);
            if (dialog == DialogResult.No)
            {
                e.Cancel = !Save();
            }
            else if(dialog == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            return e.Cancel;
        }

       
        private void btnAdd_Click(object sender, EventArgs e)
        {
            int temp0 = (int)numTemp.Value;
            double effect = (double)numEnt.Value;
            int temp1 = (int)numTempKonc.Value;
            HeatEffectDistribution heatEffectDistribution = (HeatEffectDistribution)comboBox1.SelectedItem;

            if (checkBox1.Checked)
            {
                var nowy = CalculateEnthalpy();
                Distribute(ref nowy, effect, temp0, temp1, heatEffectDistribution);
                wykresy[wykresy.Count - 1] = nowy;
            }
            else
            {
                var ent = wykresy.Last();
                Distribute(ref ent, effect, temp0, temp1, heatEffectDistribution);
                wykresy[wykresy.Count - 1] = ent;
            }
            DodajWykres(wykresy[wykresy.Count - 1]);
            if(sender != null)
            dataGridView1.Rows.Add(temp0, numTempKonc.Value, effect, heatEffectDistribution, checkBox1.Checked);
            numTemp.Minimum = numTemp.Value;
         
        }

        private void DodajWykres(Dictionary<int, double> ent)
        {
            var series = new LineSeries();
            series.Color = colors.Pop();
            if (colors.Count == 0)
                FillColors(true);

            series.MarkerType = MarkerType.Circle;
            series.MarkerSize = 1;
            series.MarkerResolution = 1;
            series.LineStyle = LineStyle.Solid;
            var points = ent.Select(x => new DataPoint(x.Key, x.Value)).OrderBy(x => x.X);
            series.Points.AddRange(points);
            plotModel.Series.Add(series);
            plotView1.Model = plotModel;
            plotView1.InvalidatePlot(false);
            if(!dodaneosie)
            {
                plotView1.Model.Axes.Add(new LinearAxis { Position = AxisPosition.Bottom, Title = "Temperatura [C]" });
                plotView1.Model.Axes.Add(new LinearAxis { Position = AxisPosition.Left, Title = "Entalpia [kJ/kg]" });
                dodaneosie = true;
            }
        }
        private bool dodaneosie = false;
        private void FillColors(bool randomSort = false)
        {
            OxyColor[] oxyColors = { OxyColors.Red, OxyColors.Blue, OxyColors.Green, OxyColors.Black, OxyColors.Yellow, OxyColors.Magenta, OxyColors.Turquoise };
            oxyColors = oxyColors.Reverse().ToArray();
            if(randomSort)
            {
                Random rand = new Random();
                oxyColors = oxyColors.OrderBy(x => rand.Next()).ToArray();
            }

            foreach (var c in oxyColors)
                colors.Push(c);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            
            Bitmap bitmap = new Bitmap(plotView1.DisplayRectangle.Width, plotView1.DisplayRectangle.Height);
            plotView1.DrawToBitmap(bitmap, plotView1.DisplayRectangle);
            DateTime time = DateTime.Now;
            string date = (time.ToShortDateString() +"-"+ time.ToShortTimeString() +"_"+ time.Second).Replace('.','_').Replace(':','_');
            string folder = Application.StartupPath;
            bitmap.Save(folder+"\\"+date+".bmp");
            MessageBox.Show("Plik został zapisany w katalogu z programem");
        }
        
        private int GaussSum(int n)
        {
            return (n * (n + 1)) / 2;
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {

            HeatEffectDistribution selected = HeatEffectDistribution.Rosnacy;
            
            if (!Enum.TryParse<HeatEffectDistribution>(comboBox1.Text, out selected) || !comboBox1.Items.Contains(selected))
            {
                comboBox1.SelectedIndex = 0;
            }
        }

        private void wyczyśćToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            OnLoad();
            numTemp.Value = numTemp.Minimum;
            numEnt.Value = 0;
        }

        private int lastTemp = 0;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox1.Checked)
            {
                lastTemp = (int)numTemp.Value;
                numTemp.Minimum = temp.First();
            }
            else
            {
                numTemp.Minimum = lastTemp;
                numTemp.Value = lastTemp;
            }
        }
    }
}
