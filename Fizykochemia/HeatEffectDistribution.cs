﻿namespace Fizykochemia
{
    public enum HeatEffectDistribution
    {
        Rosnacy = 1,
        Staly = 2,
        Malejacy = 3,
        Kwadratowy = 4,
        Sigmoidalny = 5
    }
}
